#!/bin/bash

ffmpeg -loop 1 -i watermelon_melon_fruit.jpg -filter_complex "[0:v]crop=iw/2:ih:iw/2*t/5:0,trim=duration=5" -c:a copy output.mp4
