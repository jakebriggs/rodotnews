#!/usr/bin/python
# anchor extraction from html document
from bs4 import BeautifulSoup
import urllib2
import sys
import unicodedata
import Image
import cStringIO
import os


webpage = urllib2.urlopen(str(sys.argv[1]))
tempdir = str(sys.argv[2])

# webpage = urllib2.urlopen('https://www.stuff.co.nz/national/health/102571375/poverty-causing-third-world-dental-problems-hamilton-dentist-says')
soup = BeautifulSoup(webpage,'html.parser')

# for anchor in soup.find_all('a'):
#     print(anchor.get('href', '/'))

# storybody = soup.find('article', attrs={'class': 'sics-component__story__body'})
# name = storybody.get_text()
# print name
#

# article class="story_landing"
#
# for x in soup.find_all('video'):
#   print "==================== video =================="
#   print(x)

headline = soup.find('h1', attrs={'itemprop': 'headline'}).get_text().strip()

# print("==== Headline: " + headline)

datepub = soup.find('span', attrs={'itemprop': 'datePublished'}).get_text().strip()

# print("==== Date Published: " + datepub)

storybody = soup.find('article')

# print("==== Body: " )

# find the images
c = 1

for x in storybody.find_all('img', recursive=True):
  s = x['src']
  a = x['alt']
  imgdata = urllib2.urlopen(s).read()
  img = Image.open(cStringIO.StringIO(imgdata))
  img.save(tempdir + str(c) + ".jpg")
  c += 1

print(headline + "\n")

# and print the text of the story
for x in storybody.find_all('p', recursive=False):
  if not x.contents[0].name == 'strong':
    print(x.get_text().encode('ascii', 'ignore').strip())
    # print(unicodedata.normalize("NFKD", x.get_text().strip()))
