#!/bin/bash

tempdir="/tmp/rodot/"

hls=`./findheadlines.py`

for url in $hls
do
  echo "-------------------------------"
  echo "- $url -"
  echo "-------------------------------"

  rm -rf $tempdir
  mkdir -p $tempdir

#  ./rodot.py "$url" "$tempdir" > "/tmp/x.txt"

  ./rodot.py "$url" "$tempdir" > $tempdir/rodotnews.txt

  text2wave $tempdir/rodotnews.txt -o $tempdir/rodotnews.wav
  totalduration=`soxi -D $tempdir/rodotnews.wav`
  numimages=` ls -1 $tempdir/*jpg | wc -l`

  imageduration=$(echo "$totalduration / $numimages" | bc -l)
  imagefiles=( `find $tempdir -name "*.jpg"`)

  for f in "${imagefiles[@]}"
  do
    ffmpeg -loglevel error -loop 1 -i "$f" -filter_complex "[0:v]crop=iw/2:ih:iw/2*t/$imageduration:0,trim=duration=$imageduration" -c:a copy $f.mp4
  done

  ffmpeg -loglevel error -i $tempdir/rodotnews.wav -codec:a libmp3lame -qscale:a 2 $tempdir/rodotnews.mp3

  printf "file '%s'\n" $tempdir/*.jpg.mp4 > $tempdir/vids.txt

  ffmpeg -loglevel error -f concat -safe 0 -i $tempdir/vids.txt -c copy $tempdir/output.mp4

  ffmpeg -loglevel error -i $tempdir/output.mp4 -i $tempdir/rodotnews.mp3 -codec copy  $tempdir/rodot.mp4

  title=`head -1 $tempdir/rodotnews.txt`

  echo "====================================================="
  echo "$title"
  echo "$tempdir/rodotnews.txt"
  echo "====================================================="

  youtube-upload --title="$title" --description="$headline" $tempdir/rodot.mp4
  # youtube-upload --title="$title" $tempdir/rodot.mp4
done

exit 0
