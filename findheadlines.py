#!/usr/bin/python
# anchor extraction from html document
from bs4 import BeautifulSoup
import urllib2
import sys
import unicodedata

webpage = urllib2.urlopen("https://www.stuff.co.nz/")
soup = BeautifulSoup(webpage,'html.parser')

hl = soup.find_all('a', attrs={"class": "title-headlines__headline"}, limit = 5)

print "https://www.stuff.co.nz/" + hl[0]['href'] + "\n"
print "https://www.stuff.co.nz/" + hl[1]['href'] + "\n"
print "https://www.stuff.co.nz/" + hl[2]['href'] + "\n"
print "https://www.stuff.co.nz/" + hl[3]['href'] + "\n"
print "https://www.stuff.co.nz/" + hl[4]['href'] + "\n"
